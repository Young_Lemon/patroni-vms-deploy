data "vsphere_datacenter" "dc" {
  name = "DatacenterDELL"
}

data "vsphere_datastore" "datastore" {
  name          = "DELLvsanDatastore"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_compute_cluster" "compute_cluster" {
  name          = "ClusterDELL"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name          = "ClusterDELL/Resources/Vanya"
  datacenter_id = data.vsphere_datacenter.dc.id
}
data "vsphere_network" "network" {
  name          = "DPGDELLVLAN1"
  datacenter_id = data.vsphere_datacenter.dc.id
}
data "vsphere_virtual_machine" "template" {
  name          = "tm-ubuntu-20.04-base"
  datacenter_id = data.vsphere_datacenter.dc.id
}
