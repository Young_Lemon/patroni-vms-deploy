provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

resource "local_file" "inventory" {
 filename = "./inventory"
 content = <<EOF
[etcd]
${vsphere_virtual_machine.etcd-1.name} ansible_host=${vsphere_virtual_machine.etcd-1.default_ip_address} ansible_user=ladmin ansible_ssh_pass=Gthtgjkj[
${vsphere_virtual_machine.etcd-2.name} ansible_host=${vsphere_virtual_machine.etcd-2.default_ip_address} ansible_user=ladmin ansible_ssh_pass=Gthtgjkj[
${vsphere_virtual_machine.etcd-3.name} ansible_host=${vsphere_virtual_machine.etcd-3.default_ip_address} ansible_user=ladmin ansible_ssh_pass=Gthtgjkj[
[patroni]
${vsphere_virtual_machine.patroni-1.name} ansible_host=${vsphere_virtual_machine.patroni-1.default_ip_address} ansible_user=ladmin ansible_ssh_pass=Gthtgjkj[
${vsphere_virtual_machine.patroni-2.name} ansible_host=${vsphere_virtual_machine.patroni-2.default_ip_address} ansible_user=ladmin ansible_ssh_pass=Gthtgjkj[
[haproxy]
${vsphere_virtual_machine.haproxy.name} ansible_host=${vsphere_virtual_machine.haproxy.default_ip_address} ansible_user=ladmin ansible_ssh_pass=Gthtgjkj[
EOF
}
