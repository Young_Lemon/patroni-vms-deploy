output "virtual_machine_default_ips" {
  description = "The default IP address of each virtual machine deployed, indexed by name."

  value = (zipmap(
    flatten(tolist([
      vsphere_virtual_machine.etcd-1.name,
      vsphere_virtual_machine.etcd-2.name,
      vsphere_virtual_machine.etcd-3.name,
      vsphere_virtual_machine.patroni-1.name,
      vsphere_virtual_machine.patroni-2.name,
      vsphere_virtual_machine.haproxy.name,
    ])),
    flatten(tolist([
      vsphere_virtual_machine.etcd-1.default_ip_address,
      vsphere_virtual_machine.etcd-2.default_ip_address,
      vsphere_virtual_machine.etcd-3.default_ip_address,
      vsphere_virtual_machine.patroni-1.default_ip_address,
      vsphere_virtual_machine.patroni-2.default_ip_address,
      vsphere_virtual_machine.haproxy.default_ip_address,
    ])),
  ))
}
